FROM openjdk:17 AS builder
WORKDIR /app
COPY gradlew build.gradle settings.gradle /app/
COPY gradle /app/gradle
COPY src /app/src
RUN chmod +x gradlew
RUN ./gradlew build
FROM openjdk:17
WORKDIR /app
COPY --from=builder /app/build/libs/ms10-demo1.jar app.jar
EXPOSE 8080
CMD ["java", "-jar", "app.jar"]